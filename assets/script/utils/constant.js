const BLOCK_TYPE = {
    BARRIER: 'barrier', // 障碍物
    CHARACTER: 'character', // 人物
}

export default {
    BLOCK_TYPE
}