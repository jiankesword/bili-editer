import { BLOCK_TYPE } from 'constant'

const config = [
    {
        id: 0,
        name: 'block1',
        type: BLOCK_TYPE.BARRIER,
        path: 'tield/block1',
        width: 40,
        height: 40
    },
    {
        id: 1,
        name: 'block2',
        type: BLOCK_TYPE.BARRIER,
        path: 'tield/block2',
        width: 40,
        height: 40
    },
    {
        id: 2,
        name: 'block3',
        type: BLOCK_TYPE.BARRIER,
        path: 'tield/block3',
        width: 40,
        height: 40
    },
    {
        id: 3,
        name: 'block4',
        type: BLOCK_TYPE.BARRIER,
        path: 'tield/block4',
        width: 40,
        height: 40
    },
    {
        id: 4,
        name: 'block5',
        type: BLOCK_TYPE.BARRIER,
        path: 'tield/block5',
        width: 40,
        height: 40
    },
    {
        id: 5,
        name: 'block6',
        type: BLOCK_TYPE.BARRIER,
        path: 'tield/block6',
        width: 40,
        height: 40
    },
    {
        id: 6,
        name: 'oldMan',
        type: BLOCK_TYPE.CHARACTER,
        path: 'tield/oldMan',
        width: 40,
        height: 40
    },
]

export default config